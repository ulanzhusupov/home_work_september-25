import 'dart:io';

void main(List<String> arguments) {
  task1();
//   task2();
//   task3();
//   task4();
//   task5();
//   task6();
//   task7();
//   task8();
}

// Составьте программу, выводящую на экран квадраты чисел от 10 до 20.
void task1() {
  for (int i = 10; i <= 20; i++) {
    print("$i*$i = ${i * i}");
  }
}

// Составьте программу, котораЯ вычисляет сумму чисел от 1 до n.значение n вводится с клавиатуры.
void task2() {
  print("Введите число n");
  int n = int.tryParse(stdin.readLineSync()!) ?? 1;
  int summ = 0;
  for (int i = 1; i <= n; i++) {
    summ += i;
  }

  print("Сумма всех чисел от 1 до $n = $summ");
}

// В сберкассу на трёхпроцентный вклад положили S рублей.
//Какой станет сумма вклада через N лет.(Данные вводятся с клавиатуры)
void task3() {
  print("Введите сумму для вклада:");
  int sumForDeposit = int.tryParse(stdin.readLineSync()!) ?? 0;

  print("Введите срок вклада от 1 года:");
  int yearPeriod = int.tryParse(stdin.readLineSync()!) ?? 1;

  double totalEarned = sumForDeposit.toDouble();
  for (int i = 0; i <= yearPeriod; i++) {
    totalEarned += (sumForDeposit * 0.03);
  }

  String yearString = yearPeriod == 1
      ? "год"
      : (yearPeriod >= 2 && yearPeriod <= 4 ? "года" : "лет");

  print(
      "С учетом 3% за 1 год, общая сумма за $yearPeriod $yearString станет $totalEarned");
}

// Даны натуральные числа от 20 до 50.Напечатать те из них, которые делятся на 3, но не делятся на 5.
void task4() {
  for (int i = 20; i <= 50; i++) {
    if (i % 3 == 0) {
      if (i % 5 == 0) {
        continue;
      } else {
        print(i);
      }
    }
  }
}

// Даны натуральные числа от 1 до 50. Найти сумму тех из них, которые делятся на 5 или на 7.
void task5() {
  int summ = 0;
  for (int i = 1; i <= 50; i++) {
    if (i % 5 == 0 || i % 7 == 0) {
      summ += i;
    }
  }

  print("Сумма всех чисел, которые деляться на 5 или на 7 = $summ");
}

// Напечатать те из двузначных чисел которые делятся на 4, но не делятся на 6.
void task6() {
  for (int i = 10; i < 100; i++) {
    if (i % 4 == 0) {
      if (i % 6 == 0) {
        continue;
      } else {
        print(i);
      }
    }
  }
}

// Найти сумму чисел от 100 до 200 кратных 17.
void task7() {
  int summ = 0;
  for (int i = 100; i <= 200; i++) {
    if (i % 17 == 0) {
      summ += i;
    }
  }

  print("Сумма: $summ");
}

// Составьте программу, которая вычисляет сумму квадратов чисел от 1 до введенного вами целого числа N
void task8() {
  print("Введите число:");
  int n = int.tryParse(stdin.readLineSync()!) ?? 1;
  int summ = 0;

  for (int i = 1; i <= n; i++) {
    summ += (i * i);
  }

  print("Сумма всех квадратов = $summ");
}
